package travelplusplus.app_backend;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import travelplusplus.app_backend.model.Example;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("travel")
public class ApiResource {
	
	/**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Example getIt() {
    	Example ex = new Example("Olivier", 30);
        return ex;
    }

}
